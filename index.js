import express from "express";
import cors from "cors";
import axios from "axios";

const PORT = 8080;
const app = express();

app.use(cors());

const client = axios.create({
  baseURL: "http://localhost:8081",
});


/**
 * Redirecciona las peticiones al servidor que gestiona los carritos de la compra
 */
app.get("/checkStock", async (req, res) => {
  const productName = req.query.productName;
  console.log("Checking stock for product: " + productName);

  try {
    const response = await client.get("/checkStock", {
      params: { productName },
    });
    const product = response.data.product;
    res.status(200).send({ product });
  } catch (error) {
    res.sendStatus(404);
  }
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
