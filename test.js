import axios from "axios";

import ShoppingCart from "./public/ShoppingCart.js";

const productNames = [
  "naranja",
  "mosto",
  "helado",
  "tomate",
  "macarrones",
  "pollo",
  "judia",
];

//Cliente axios
const client = axios.create({
  baseURL: "http://localhost:8080",
});

/**
 * Realiza una petición al servicio para obtener un producto
 * @param {string} name nombre del producto
 * @returns el producto buscado con nombre 'name'
 */
async function getProduct(name) {
  try {
    const response = await client.get("/checkStock", {
      params: { productName: name },
    });
    return response.data.product;
  } catch (error) {
    return 0;
  }
}

/**
 * Función de test que intenta añadir productos a un carrito,
 * retirarlos, y verificar su stock
 */
async function run() {
  let shoppingCart = new ShoppingCart(0);

  //Añadir cada producto de la lista productNames al carrito, uno a uno
  for (let i = 0; i < productNames.length; i++) {
    //Llama al servidor para verificar si este producto tiene stock
    const product = await getProduct(productNames[i]);
    //Verifica si el producto existe en la base de datos
    if (!product) {
      console.log(`El producto ${productNames[i]} no existe`);
    } else if (product.stock == 0) {
      console.log(
        `No quedan más unidades del producto: ${productNames[i]}`
      );
    } else {
      if (
        shoppingCart.getProductAmount(productNames[i]) <
        product.stock
      ) {
        shoppingCart.addProduct(product);
        console.log(
          `El producto ${productNames[i]} (stock: ${product.stock}) se ha añadido al carrito`
        );
      } else {
        console.log(
          `No quedan más unidades del producto: ${productNames[i]}`
        );
      }
    }
  }
  //Imprime el contenido del carrito
  console.log(shoppingCart.toString());

  //Retira cada producto de la lista del carrito, uno a uno
  for (let i = 0; i < productNames.length; i++) {
    if (shoppingCart.removeProduct(productNames[i]))
    {
      console.log(`El producto ${productNames[i]} se ha retirado del carrito`);
    }
  }
  //Imprime el contenido del carrito (será vacio)
  console.log(shoppingCart.toString());
}

/**
 * Funcción principal
 */
function main() {
  run();
}

main();
