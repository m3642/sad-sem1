Entorno utilizado:
    - node 14.18.0
    - npm 6.14.15

Sugerencia de uso de git
 - Los cambios se hacen en la rama 'develop'
 - Si vamos a añadir funcionalidad, creamos una rama 'nombre de funcionalidad'
 - Cuando se termina el cambio en la rama, se hace merge con la rama 'develop'
 - Después de asegurarnos de que 'develop' funciona, hacemos merge con la rama 'master'

IMPORTANTE DESCARGAR DEPENDENCIAS DEL PROYECTO ANTES DE EJECUTARLO

Descargar dependencias del proyecto
> npm install

Ejecución del servidor HTTP principal
> npm run main

Ejecución del servidor HTTP secundario: gestiona peticiones de stock
> npm run secondary

Ejecución de script de prueba
> npm run test

Actualizar base de datos
> npm run updatedb

URL servidor principal: http://localhost:8080
URL servidor secundario: http://localhost:8081