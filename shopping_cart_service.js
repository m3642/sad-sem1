import express from "express";

import db from "./db_service.js";

const PORT = 8081;
const app = express();

/**
 * Llama a la base de datos para verificar si un producto tiene stock.
 * Si este producto existe en la base de datos, devuelve el producto (con su id, su nombre y su stock).
 */
app.get("/checkStock", async (req, res) => {
  const productName = req.query.productName;
  console.log("Checking stock for product: " + productName);

  try {
    const product = await db.getProductByName(productName);
    res.status(200).send({ product });
  } catch (error) {
    res.sendStatus(404);
  }
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
