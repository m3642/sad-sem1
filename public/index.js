import ShoppingCart from "./ShoppingCart.js";

const url = "http://localhost:8080";

//Añade EventListeners a los botones presentes en el HTML
document.getElementById("addProduct").addEventListener("click", addProduct);
document
  .getElementById("removeProduct")
  .addEventListener("click", removeProduct);
document.getElementById("checkStock").addEventListener("click", checkStock);

var shopping_cart = new ShoppingCart(0);

/**
 * Función llamada cuando el usuario quiere añadir un producto al carrito
 */
function addProduct() {
  //El nombre del producto está en el HTML
  const productName = document.getElementById("product").value;
  //Llama al servidor
  fetch(`${url}/checkStock?productName=${productName}`)
    .then((res) => res.json())
    .then((res) => {
      if (!res.product) {
        updateAlert(true, `El producto ${productName} no existe`);
      } else if (res.product.stock == 0) {
        updateAlert(
          true,
          `No quedan más unidades del producto: ${productName}`
        );
      } else {
        if (shopping_cart.getProductAmount(productName) < res.product.stock) {
          updateAlert(
            false,
            `El producto ${productName} se ha añadido al carrito`
          );
          shopping_cart.addProduct(res.product);
          document.getElementById("cartString").textContent =
            shopping_cart.toString();
        } else {
          updateAlert(
            true,
            `No quedan más unidades del producto: ${productName}`
          );
        }
      }
    })
    .catch(console.error);
}

/**
 * Función llamada cuando el usuario quiere retirar un producto del carrito
 */
function removeProduct() {
  const productName = document.getElementById("product").value;
  shopping_cart.removeProduct(productName);
  document.getElementById("cartString").textContent = shopping_cart.toString();
}

/**
 * Función llamada cuando el usuario quiere verificar el stock de un producto
 */
function checkStock() {
  const productName = document.getElementById("product").value;
  fetch(`${url}/checkStock?productName=${productName}`)
    .then((res) => res.json())
    .then((res) => {
      if (!res.product) {
        updateAlert(true, `El producto ${productName} no existe`);
      } else if (res.product.stock == 0) {
        updateAlert(
          true,
          `No quedan más unidades del producto: ${productName}`
        );
      } else {
        updateAlert(
          false,
          `Quedan ${res.product.stock} unidades del producto ${productName}`
        );
      }
    })
    .catch(console.error);
}

/**
 * Actualiza la alerta presente en el HTML con un mensaje personalizado
 * @param {boolean} danger - indica si es una alera negativa o no
 * @param {string} message - contenido de la alerta
 */
function updateAlert(danger, message) {
  console.log(message);
  let alert = document.getElementById("alert");
  if (danger) {
    if (alert.classList.contains("alert-success")) {
      alert.classList.remove("alert-success");
    }
    alert.classList.add("alert-danger");
  } else {
    if (alert.classList.contains("alert-danger")) {
      alert.classList.remove("alert-danger");
    }
    alert.classList.add("alert-success");
  }
  alert.textContent = message;
}
