import { MongoClient } from "mongodb";

//Credenciales de MongoDB 
const uri =
  "mongodb+srv://user:pssw0rd7@database-cl0.ns16z.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const client = new MongoClient(uri);

const docs = [
  { _id: 1, name: "zumo", stock: 5 },
  { _id: 2, name: "mosto", stock: 2 },
  { _id: 3, name: "helado", stock: 7 },
  { _id: 4, name: "tomate", stock: 12 },
  { _id: 5, name: "macarrones", stock: 0 },
  { _id: 6, name: "pollo", stock: 3 },
];

/**
 * Llena la base de datos con productos (que contienen un id, un nombre y un stock)
 */
async function fillDB() {
  try {
    //Conexión a la base de datos
    console.log("Connecting to database...");
    await client.connect();
    console.log("Connected to database");

    //Borra los documentos presentes en la base de datos
    const db = client.db("stock");
    const products = db.collection("products");
    await products.drop();

    //Inserta los documentos en la base de datos
    const result = await products.insertMany(docs);
    console.log(`${result.insertedCount} documents were inserted`);
  } finally {
    await client.close();
  }
}

fillDB().catch(console.dir);
